let hal_urls = new Map([
    ["ouvrages", "https://api.archives-ouvertes.fr/search/?q=*:*&fq=docType_s:(OUV)&rows=4&fq=authIdHal_s:mathieu-goux&sort=publicationDate_tdate+desc"],
    ["articles", "https://api.archives-ouvertes.fr/search/?q=*:*&fq=docType_s:(ART)&rows=4&fq=authIdHal_s:mathieu-goux&sort=publicationDate_tdate+desc"]
]);

function getHalInfo() {
    hal_urls.forEach(function (hal_url, title) {
        fetch(hal_url)
            .then(response => response.json())
            .then((json) => displayHalInfo(title, json))
    });
}

function displayHalInfo(title, json) {
    let div = document.getElementById("publications");
    let innerdiv = document.createElement("div");
    innerdiv.className = "publication";
    let list = document.createElement("ul");
    let h2 = document.createElement("h2");
    h2.innerText = title;
    div.appendChild(innerdiv);
    innerdiv.appendChild(h2);

    json.response.docs.forEach((doc) => {
        let [authors, title, ref] = splitTitle(doc.label_s);
        let a = document.createElement("a");
        a.innerHTML = ref;
        a.className = "ref";
        a.href = doc.uri_s;
        let p = document.createElement("p");
        p.innerHTML = title;
        p.className = "title";
        let span = document.createElement("span");
        span.className = "authors";
        span.innerHTML = authors;
        let li = document.createElement("li");
        li.appendChild(a);
        li.appendChild(p);
        li.appendChild(span);
        list.appendChild(li);
        innerdiv.appendChild(list);
    });
}

function splitTitle(fullTitle) {
    parts = fullTitle.split(". ");
    authors = parts.shift();
    ref = parts.pop();
    title = parts.join(". ");

    return [
        authors,
        title,
        ref
    ];
}

window.onload = () => {
    getHalInfo();
}